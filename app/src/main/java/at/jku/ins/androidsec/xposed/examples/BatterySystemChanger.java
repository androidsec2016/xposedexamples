package at.jku.ins.androidsec.xposed.examples;


import android.os.BatteryManager;

import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;

/**
 * Created by Gerald on 12/01/2017.
 */

public class BatterySystemChanger implements IXposedHookLoadPackage {

    public void handleLoadPackage(LoadPackageParam lpparam) throws Throwable {

       findAndHookMethod("android.content.Intent", lpparam.classLoader, "getIntExtra",String.class
               ,int.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {

                //check if it is the value that we are interested in
               if(!param.args[0].equals(BatteryManager.EXTRA_LEVEL) )
                    return;
                //set the return value to a fixed value
                param.setResult((Integer) 2);
            }
        });
    }
}
