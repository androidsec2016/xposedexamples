package at.jku.ins.androidsec.xposed.examples;


import java.lang.reflect.Field;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

/**
 * Created by Gerald on 12/01/2017.
 */

public class BatteryUIChanger  implements IXposedHookLoadPackage {

    public void handleLoadPackage(LoadPackageParam lpparam) throws Throwable {

        //for manipulating the status bar we are only interested in process loading packages from com.android.systemui
        if (!lpparam.packageName.equals("com.android.systemui"))
            return;

        //find private BatteryTracker method
        findAndHookMethod("com.android.systemui.BatteryMeterView$BatteryTracker", lpparam.classLoader
                , "onReceive","android.content.Context","android.content.Intent", new XC_MethodHook() {

            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                //find the private level field of the private nested class using reflection and alter it
                Field field = param.thisObject.getClass().getDeclaredField("level");
                field.setAccessible(true);
                field.setInt(param.thisObject,50);
            }
        });
    }
}
